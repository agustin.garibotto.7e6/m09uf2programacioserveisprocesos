﻿using System.Threading;
namespace provaTascaThreads
{
    class ProvaThreads
    {
        static void Main()
        {
            Nevera nevera = new Nevera(6);

            Thread thread1 = new Thread(() => nevera.OmplirNevera("Annita"));
            Thread thread2 = new Thread(() => nevera.BeberCerveza("Bad Bunny"));
            Thread thread3 = new Thread(() => nevera.BeberCerveza("Lil Nas X"));
            Thread thread4 = new Thread(() => nevera.BeberCerveza("Manuel Turizo"));

            Thread[] procesos = { thread1, thread2, thread3, thread4 };
            foreach (Thread hilo in procesos)
            {
                hilo.Start();
                hilo.Join();
            }
        }
    }
}
