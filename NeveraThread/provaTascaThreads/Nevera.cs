﻿using System;

namespace provaTascaThreads
{
    class Nevera
    {
        public static Random Rnd = new Random();
        public int Cervezas { get; set; }
        public Nevera(int cerveces)
        {
            Cervezas = cerveces;
        }

        public void OmplirNevera(string name)
        {
            Cervezas += Rnd.Next(0, 7);
            if (Cervezas > 9) Cervezas = 9;
            Console.WriteLine(name + " llena la nevera y deja " + Cervezas + " cervezas");
               
        }
        public void BeberCerveza(string name)
        {
            int cervezasBebidas = Rnd.Next(1, 7);
            if (Cervezas - cervezasBebidas < 0)
            {
                string quedaban = Cervezas > 0 ? $" pero solo ha podido beber {Cervezas}" : " pero no quedaban";
                Console.WriteLine(name + " quería beber: " + cervezasBebidas + quedaban);
                Cervezas = 0;
            }
            else
            {
                Cervezas -= cervezasBebidas;
                if (Cervezas < 0) Cervezas = 0;
                Console.WriteLine(name + $" bebe {cervezasBebidas}");
            }

        }

    }
}
