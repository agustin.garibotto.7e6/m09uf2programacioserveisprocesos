﻿using System;
using System.Threading;

namespace CenaFilosofos
{
    class Filosofo
    {
        private readonly string Name;
        private readonly Palillo PalilloIzq;
        private readonly Palillo PalilloDer;
        private readonly Random Rnd = new Random();

        public Filosofo(string name, Palillo palilloIzq, Palillo palilloDer)
        {
            Name = name;
            PalilloIzq = palilloIzq;
            PalilloDer = palilloDer;
        }

        public void Cenar()
        {
            while (true)
            {
                Pensar();
                lock (PalilloIzq)
                {
                    Console.WriteLine($"{Name} tiene el palillo izquierdo {PalilloIzq.Id}");
                    Thread.Sleep(Rnd.Next(100, 3000)); // Espera un tiempo para coger el palillo derecho

                    if (Monitor.TryEnter(PalilloDer, TimeSpan.FromSeconds(2))) // Intenta bloquear el palillo derecho durante 2 segundos, si no lo consigue devuelve false
                    {
                        try
                        {
                            Console.WriteLine($"{Name} ha cogido el palillo derecho {PalilloDer.Id} y está comiendo...");
                            Thread.Sleep(Rnd.Next(500, 5000)); // El filósofo come por un tiempo aleatorio
                        }
                        finally 
                        {
                            Console.WriteLine($"{Name} ha soltado el palillo derecho {PalilloDer.Id}");
                            Monitor.Exit(PalilloDer); // Se libera el palillo derecho      
                        }
                    }
                    else // Si no ha podido coger el palillo derecho tendrá hambre y soltará el palillo izquierdo
                    {      
                        Console.WriteLine($"{Name} tiene hambre...");
                        Thread.Sleep(Rnd.Next(100, 2000));
                    }
                    Console.WriteLine($"{Name} ha soltado el palillo izquierdo {PalilloIzq.Id}"); // Se libera el palillo izquierdo
                }
            }
        }
        public void Pensar()
        {
            Console.WriteLine($"{Name} está pensando en cuándo coger los palillos {PalilloIzq.Id} - {PalilloDer.Id}");
            Thread.Sleep(Rnd.Next(100, 3000)); // Piensa un tiempo aleatorio
        }
    }
}
