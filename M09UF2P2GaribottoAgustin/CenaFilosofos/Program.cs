﻿using System.Threading;

namespace CenaFilosofos
{
    class Program
    {
        static void Main()
        {
            int numFilosofos = 5;
            Filosofo[] filosofos = new Filosofo[numFilosofos];
            Palillo[] palillos = new Palillo[numFilosofos];
            string[] nombres = { "socrates", "aristoteles", "platón", "descartes", "kant" };
            // Creamos los palillos
            for (int i = 0; i < numFilosofos; i++)
            {
                palillos[i] = new Palillo(i);
            }
            // Asignamos los palillos a los filosofos
            for (int i = 0; i < numFilosofos; i++)
            {
                Palillo palilloIzq = palillos[i];
                Palillo palilloDer = palillos[(i + 1) % numFilosofos];
                filosofos[i] = new Filosofo(nombres[i], palilloIzq, palilloDer);
            }
            // Iniciamos los hilos de los filosofos
            foreach (Filosofo filosofo in filosofos)
            {
                Thread t = new Thread(new ThreadStart(filosofo.Cenar));
                t.Start();
            }
        }
    }
}
