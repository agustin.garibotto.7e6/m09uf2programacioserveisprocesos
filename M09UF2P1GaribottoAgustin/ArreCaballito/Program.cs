﻿using System;
/*
    DADES
    P1 - Threads
    NOM: Agustín Garibotto Villanueva
    DATA: 26/05/2023
*/
namespace ArreCaballito
{
    class Program
    {
       
        static void Main()
        {       
            Console.WriteLine("Apreta la tecla C para empezar la carrera!");
            while (Console.ReadKey().Key != ConsoleKey.C) ;
            Console.Clear();
            Carrera carrera = new Carrera();
            carrera.StartRace();
            carrera.MostrarGanador();
        }

       

    }
}
