﻿using System;
using System.Threading;

namespace ArreCaballito
{
    class Carrera
    {
        // Array de 6 caballos
        public static Caballo[] establo = { new Caballo("Popeye"), new Caballo("Juanito"), new Caballo("Callabo"), new Caballo("Wingardium"), new Caballo("Tortuga"), new Caballo("Pantera") };

        // Bool que determina si el caballo ha ganado la carrera
        public string CaballoWinner { get; set; }

        // Método iniciar carrera
        public void StartRace()
        {
            foreach (Caballo caballo in establo) caballo.Hilo.Start();
            bool finCarrera = false;
            string caballoGanador = "";
            do
            {
                foreach (Caballo caballo in establo)
                {
                    Console.Write(caballo.Name + " :\t" + caballo.InitialPos + ">\n");
                    if (caballo.FinishedRace && !finCarrera)
                    {
                        finCarrera = caballo.FinishedRace;
                        caballoGanador = caballo.Name;
                    }
                }
                if (!finCarrera)
                {
                    Thread.Sleep(500);
                    Console.Clear();
                }
            } while (!finCarrera);
            CaballoWinner = caballoGanador;
        }
        public void MostrarGanador()
        {
            Console.WriteLine($"El ganador es {CaballoWinner} !");
            Environment.Exit(0);
        }

    }
}
