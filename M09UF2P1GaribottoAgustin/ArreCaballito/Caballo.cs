﻿using System;
using System.Threading;


namespace ArreCaballito
{
    class Caballo
    {
        public string Name { get; set; }
        public string InitialPos = "";
        public readonly static Random Rnd = new Random();
        public int Speed { get; set; }
        public Thread Hilo { get; set; }
        public bool FinishedRace { get; set; }
        public Caballo(string name)
        {
            Name = name;
            Hilo = new Thread(() => { Avanzar(); });
        }
        public void Avanzar()
        {
            // Como variable para determinar la velocidad utilizo el tiempo variable de manera aleatoria, y la distancia constante representada con un guión
            while (InitialPos.Length < 20)
            {
                InitialPos += "-";
                Speed = Rnd.Next(100, 3001); // Para que haya remontadas y no gane "automáticamente" quien tenga más velocidad
                Thread.Sleep(Speed); 
            }
            FinishedRace = true;
        }

    }
}
